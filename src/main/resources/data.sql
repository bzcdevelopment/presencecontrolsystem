-- DEPARTMENT's table dummy records ----------------------------------------------------------------

INSERT INTO DEPARTMENT (DEPARTMENT_ID, NAME) VALUES (1, 'Desarrollo de producto');
INSERT INTO DEPARTMENT (DEPARTMENT_ID, NAME) VALUES (2, 'Recursos humanos');


-- EMPLOYEE's table dummy records ------------------------------------------------------------------

INSERT INTO EMPLOYEE (EMPLOYEE_ID, DEPARTMENT_ID, DOCUMENT_TYPE, DOCUMENT, FIRST_NAME, SURNAME, ENABLE, FINGERPRINT, PRESENCE_CHECK_STATUS) VALUES (1, 1, 'DNI', '8768687F', 'Pedro', 'Garcia', true, 'FP1', 'out');
INSERT INTO EMPLOYEE (EMPLOYEE_ID, DEPARTMENT_ID, DOCUMENT_TYPE, DOCUMENT, FIRST_NAME, SURNAME, ENABLE, FINGERPRINT, PRESENCE_CHECK_STATUS) VALUES (2, 1, 'DNI', '2323234G', 'Gonzalo', 'Jimenez', true,  'FP2', 'out');
INSERT INTO EMPLOYEE (EMPLOYEE_ID, DEPARTMENT_ID, DOCUMENT_TYPE, DOCUMENT, FIRST_NAME, SURNAME, ENABLE, FINGERPRINT, PRESENCE_CHECK_STATUS) VALUES (3, 2, 'DNI', '6786784A', 'Ana', 'Portilla', true, 'FP3', 'out');
INSERT INTO EMPLOYEE (EMPLOYEE_ID, DEPARTMENT_ID, DOCUMENT_TYPE, DOCUMENT, FIRST_NAME, SURNAME) VALUES (4, 1, 'DNI', '0809688F', 'Alvaro', 'Nuñez');
INSERT INTO EMPLOYEE (EMPLOYEE_ID, DEPARTMENT_ID, DOCUMENT_TYPE, DOCUMENT, FIRST_NAME, SURNAME) VALUES (5, 2, 'DNI', '1124234Z', 'Sandra', 'Malaya');


-- ACCESS_DEVICE's table dummy records ------------------------------------------------------------


INSERT INTO ACCESS_DEVICE (ACCESS_DEVICE_ID, DEPARTMENT_ID, CHECK_ACCESS) VALUES (1, 1, 'IN');
INSERT INTO ACCESS_DEVICE (ACCESS_DEVICE_ID, DEPARTMENT_ID, CHECK_ACCESS) VALUES (2, 1, 'OUT');
INSERT INTO ACCESS_DEVICE (ACCESS_DEVICE_ID, DEPARTMENT_ID, CHECK_ACCESS) VALUES (3, 2, 'IN');
INSERT INTO ACCESS_DEVICE (ACCESS_DEVICE_ID, DEPARTMENT_ID, CHECK_ACCESS) VALUES (4, 2, 'OUT');


-- ACCESS_HISTORY's table dummy records -----------------------------------------------------------

--INSERT INTO ACCESS_HISTORY (EMPLOYEE_ID, DEPARTMENT_ID, ACCESS_DEVICE_ID, TYPE_ACCESS) VALUES (1,1,1,1);


-- WORKDAY's table dummy records ------------------------------------------------------------------

INSERT INTO WORKDAY (WORKDAY_ID, WORKDAY_TYPE, CHECK_IN_TIME, CHECK_OUT_TIME, BREAK_BEGIN_TIME, BREAK_FINISH_TIME, VALID_MONTHS, VALID_DAYS, ENABLE) VALUES (1, 'PART_TIME', '09:00:00', '19:00:00', '14:00:00', '16:00:00', '1,2,3,4,5,6,9,10,11,12', '1,2,3,4', true);
INSERT INTO WORKDAY (WORKDAY_ID, WORKDAY_TYPE, CHECK_IN_TIME, CHECK_OUT_TIME, BREAK_BEGIN_TIME, BREAK_FINISH_TIME, VALID_MONTHS, VALID_DAYS, ENABLE) VALUES (2, 'FULL_TIME', '09:00:00', '17:00:00', '14:00:00', '14:30:00', '1,2,3,4,5,6,9,10,11,12', '5,6,7', false);
--INSERT INTO WORKDAY (WORKDAY_ID, WORKDAY_TYPE, CHECK_IN_TIME, CHECK_OUT_TIME, BREAK_BEGIN_TIME, BREAK_FINISH_TIME, VALID_MONTHS, VALID_DAYS, ENABLE) VALUES (1, 'PART_TIME', '09:00:00', '19:00:00', '14:00:00', '16:00:00', (1,2,3,4,5,6,9,10,11,12), (1,2,3,4), true);
--INSERT INTO WORKDAY (WORKDAY_ID, WORKDAY_TYPE, CHECK_IN_TIME, CHECK_OUT_TIME, BREAK_BEGIN_TIME, BREAK_FINISH_TIME, VALID_MONTHS, VALID_DAYS, ENABLE) VALUES (2, 'FULL_TIME', '09:00:00', '17:00:00', '14:00:00', '14:30:00', (1,2,3,4,5,6,9,10,11,12), (5), false);
INSERT INTO WORKDAY (WORKDAY_ID, WORKDAY_TYPE, CHECK_IN_TIME, CHECK_OUT_TIME, BREAK_BEGIN_TIME, BREAK_FINISH_TIME, VALID_MONTHS, VALID_DAYS, ENABLE) VALUES (3, 'INTENSIVE', '09:00:00', '15:00:00', '14:00:00', '14:30:00', '7,8', '1,2,3,4,5,6,7', false);
INSERT INTO WORKDAY (WORKDAY_ID, WORKDAY_TYPE, CHECK_IN_TIME, CHECK_OUT_TIME, BREAK_BEGIN_TIME, BREAK_FINISH_TIME, VALID_MONTHS, VALID_DAYS, ENABLE) VALUES (4, 'YEAR_INTENSIVE', '09:00:00', '15:00:00', '14:00:00', '14:30:00', '1,2,3,4,5,6,7,8,9,10,11,12', '1,2,3,4,5', true);

-- DEPARTMENT_SCHEDULE's table dummy records ------------------------------------------------------

INSERT INTO DEPARTMENT_SCHEDULE (DEPARTMENT_ID, WORKDAY_ID) VALUES (1,1);
INSERT INTO DEPARTMENT_SCHEDULE (DEPARTMENT_ID, WORKDAY_ID) VALUES (1,2);
INSERT INTO DEPARTMENT_SCHEDULE (DEPARTMENT_ID, WORKDAY_ID) VALUES (1,3);
INSERT INTO DEPARTMENT_SCHEDULE (DEPARTMENT_ID, WORKDAY_ID) VALUES (2,4);



