package com.stratio.challenge.controller;


import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.entity.AccessHistory;
import com.stratio.challenge.exception.BackendException;
import com.stratio.challenge.service.AccessControlServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/access")
public class AccessController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessController.class);

    @Autowired
    private AccessControlServiceImpl accessControlService;

    @PostMapping(path = "/in")
    public AccessHistory employeeCheckIn(@RequestParam Integer accessDeviceId, @RequestParam String fingerprint) throws BackendException
    {
        return accessControlService.registerEmployeeCheckAccess(accessDeviceId, fingerprint, PresenceCheck.IN);
    }

    @PostMapping(path = "/out")
    public AccessHistory EmployeeCheckOut(@RequestParam Integer accessDeviceId, @RequestParam String fingerprint) throws BackendException
    {
        return accessControlService.registerEmployeeCheckAccess(accessDeviceId, fingerprint, PresenceCheck.OUT);
    }
}
