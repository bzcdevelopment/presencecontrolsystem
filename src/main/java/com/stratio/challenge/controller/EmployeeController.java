package com.stratio.challenge.controller;

import com.stratio.challenge.entity.Employee;
import com.stratio.challenge.exception.BackendException;
import com.stratio.challenge.service.EmployeeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeServiceImpl employeeService;

    @GetMapping(path = "/{id}")
    public Employee getEmployeeById(@PathVariable Integer id) throws BackendException
    {
        return employeeService.findEmployeeById(id);
    }
}
