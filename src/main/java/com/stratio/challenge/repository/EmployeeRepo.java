package com.stratio.challenge.repository;

import com.stratio.challenge.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Optional;


@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer>, EmployeeRepoCustom {

    Optional<Employee> findEmployeeById(Integer id);

    Optional<Employee> findEmployeeByFingerprint(String fingerPrint);
}
