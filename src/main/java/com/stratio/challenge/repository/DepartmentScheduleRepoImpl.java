package com.stratio.challenge.repository;

import com.stratio.challenge.entity.Workday;
import com.stratio.challenge.exception.PersistenceException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class DepartmentScheduleRepoImpl implements DepartmentScheduleRepoCustom{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Workday> findAllByDepartmentId(Integer departmentId) throws PersistenceException {

        List<Workday> workdays;

        try {


            Query query = entityManager.createNativeQuery(  "SELECT  WDAY.WORKDAY_ID, " +
                                                                    "WDAY.WORKDAY_TYPE, " +
                                                                    "WDAY.CHECK_IN_TIME, " +
                                                                    "WDAY.CHECK_OUT_TIME, " +
                                                                    "WDAY.BREAK_BEGIN_TIME, " +
                                                                    "WDAY.BREAK_FINISH_TIME, " +
                                                                    "WDAY.VALID_MONTHS, " +
                                                                    "WDAY.VALID_DAYS, " +
                                                                    "WDAY.ENABLE " +
                                                            "FROM DEPARTMENT_SCHEDULE AS SCHEDULE " +
                                                            "JOIN WORKDAY AS WDAY ON SCHEDULE.WORKDAY_ID = WDAY.WORKDAY_ID " +
                                                            "WHERE SCHEDULE.DEPARTMENT_ID = ?", Workday.class);

            query.setParameter(1, departmentId);

            workdays = query.getResultList();
        }catch (Exception e){
            throw new PersistenceException(e.getMessage(), e);
        }

        return workdays;
    }
}
