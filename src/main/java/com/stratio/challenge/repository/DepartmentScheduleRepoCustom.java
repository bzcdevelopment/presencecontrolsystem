package com.stratio.challenge.repository;

import com.stratio.challenge.entity.Workday;
import com.stratio.challenge.exception.PersistenceException;

import java.io.Serializable;
import java.util.List;

public interface DepartmentScheduleRepoCustom extends Serializable {

    List<Workday> findAllByDepartmentId(Integer departmentId) throws PersistenceException;
}
