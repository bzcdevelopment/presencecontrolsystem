package com.stratio.challenge.repository;

import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.exception.PersistenceException;

import java.io.Serializable;

public interface EmployeeRepoCustom extends Serializable{

    void updatePresenceCheckStatus(Integer employeeId, PresenceCheck checkStatus) throws PersistenceException;

}
