package com.stratio.challenge.repository;

import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.entity.Employee;
import com.stratio.challenge.exception.PersistenceException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class EmployeeRepoImpl implements EmployeeRepoCustom {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public void updatePresenceCheckStatus(Integer employeeId, PresenceCheck checkStatus) throws PersistenceException {
        try{
            Query query = entityManager.createNativeQuery(  "UPDATE  EMPLOYEE SET PRESENCE_CHECK_STATUS = ? " +
                                                            "WHERE EMPLOYEE_ID = ?", Employee.class);

            query.setParameter(1, checkStatus.getLabel());
            query.setParameter(2, employeeId);

            query.executeUpdate();
        }catch (Exception e){
            throw new PersistenceException(e.getMessage(), e);
        }
    }
}
