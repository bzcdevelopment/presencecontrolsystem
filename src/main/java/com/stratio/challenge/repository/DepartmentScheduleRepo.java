package com.stratio.challenge.repository;

import com.stratio.challenge.entity.DepartmentSchedule;
import com.stratio.challenge.entity.DepartmentScheduleId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DepartmentScheduleRepo extends JpaRepository<DepartmentSchedule, DepartmentScheduleId>,
                                                DepartmentScheduleRepoCustom {

}
