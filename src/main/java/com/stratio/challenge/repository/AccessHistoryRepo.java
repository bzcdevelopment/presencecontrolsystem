package com.stratio.challenge.repository;

import com.stratio.challenge.entity.AccessHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessHistoryRepo extends  JpaRepository<AccessHistory, Integer>,
                                            AccessHistoryRepoCustom
{


}
