package com.stratio.challenge.service;

import com.stratio.challenge.commons.utils.DateUtils;
import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.entity.AccessHistory;
import com.stratio.challenge.entity.AccessHistoryId;
import com.stratio.challenge.entity.Employee;
import com.stratio.challenge.entity.Workday;
import com.stratio.challenge.exception.BackendException;
import com.stratio.challenge.exception.BusinessException;
import com.stratio.challenge.helper.SecurityCheckHelper;
import com.stratio.challenge.repository.AccessHistoryRepo;
import com.stratio.challenge.repository.DepartmentScheduleRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;


@Service
public class AccessControlServiceImpl implements AccessControlService {

    private static final long serialVersionUID = 7856310005478869408L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessControlServiceImpl.class);

    // =====================================================================
    // ATTRIBUTES
    // =====================================================================

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private AccessHistoryRepo historyRepo;

    @Autowired
    private DepartmentScheduleRepo departmentScheduleRepo;

    @Autowired
    private SecurityCheckHelper securityCheckHelper;

    // =====================================================================
    // INNER IMPLEMENTATIONS
    // =====================================================================

    @Transactional
    @Override
    public AccessHistory registerEmployeeCheckAccess(Integer accessDeviceId, String fingerprint, PresenceCheck check) throws BackendException {

        AccessHistory accessRecord = new AccessHistory();
        Employee employee = employeeService.findEmployeeByFingerprint(fingerprint);

        // employee's status validation --------------------------------------------------------------------------------
        securityCheckHelper.isEmployeeEnabled(employee.getEnable());
        PresenceCheck currentCheckStatus =  PresenceCheck.valueOf(employee.getPresenceCheckStatus().toUpperCase());

        // current employee's status check has to be different to the new check (in-out) status ------------------------
        securityCheckHelper.employeePresenceCheckStatusValidator(currentCheckStatus, check);

        //Retrieve us the workdays of the employee's department --------------------------------------------------------
        List<Workday> workdays = departmentScheduleRepo.findAllByDepartmentId(employee.getDepartmentId());

        Calendar currentDate = Calendar.getInstance();

        Workday workday = workdays.stream()
                                .filter(w -> w.getValidMonthsList().contains(currentDate.get(Calendar.DAY_OF_WEEK)) &&
                                             w.getValidDaysList().contains(currentDate.get(Calendar.MONTH)+1))
                                .findFirst()
                                .orElseThrow(() -> new BusinessException("Missing workday configuration"));

        securityCheckHelper.workdayTimeWindowValidator( check,
                                                        currentDate,
                                                        DateUtils.getInstance().getDateWithThisTime(workday.getCheckInTime()));

        accessRecord = new AccessHistory();
        accessRecord.setAccessHistoryId(new AccessHistoryId(employee.getId(), employee.getDepartmentId(), accessDeviceId));
        accessRecord.setCheckAccess(check.getLabel());
        accessRecord.setFlagCheck(check.getFlag());
        accessRecord.setDateAccess(DateUtils.getInstance().getCalendar().getTime());

        // Save access record ------------------------------------------------------------------------------------------
        accessRecord = historyRepo.saveAndFlush(accessRecord);

        // Update employee's check status ------------------------------------------------------------------------------
        employeeService.updatePresenceCheckStatus(employee.getId(), check);

        return accessRecord;
    }
}
