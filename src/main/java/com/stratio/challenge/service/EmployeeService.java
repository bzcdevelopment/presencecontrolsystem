package com.stratio.challenge.service;

import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.entity.Employee;
import com.stratio.challenge.exception.BackendException;

import java.io.Serializable;

public interface EmployeeService extends Serializable {

    Employee findEmployeeById(Integer employeeId) throws BackendException;

    Employee findEmployeeByFingerprint(String fingerPrint) throws BackendException;

    void updatePresenceCheckStatus(Integer employeeId, PresenceCheck checkStatus)throws BackendException;


}
