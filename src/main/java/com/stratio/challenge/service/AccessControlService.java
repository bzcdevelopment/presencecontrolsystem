package com.stratio.challenge.service;

import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.entity.AccessHistory;
import com.stratio.challenge.exception.BackendException;

import java.io.Serializable;

public interface AccessControlService extends Serializable {

    AccessHistory registerEmployeeCheckAccess(Integer accessDeviceId, String fingerprint, PresenceCheck check) throws BackendException;
}
