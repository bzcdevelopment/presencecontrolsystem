package com.stratio.challenge.service;

import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.entity.Employee;
import com.stratio.challenge.exception.BackendException;
import com.stratio.challenge.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stratio.challenge.exception.BusinessException;


@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final long serialVersionUID = -1735160201517504L;

    @Autowired
    private EmployeeRepo employeeRepo;

    @Override
    public Employee findEmployeeById(Integer employeeId) throws BackendException {
       return employeeRepo.findEmployeeById(employeeId)
                            .orElseThrow(() -> new BusinessException("Employee doesn't exist"));
    }

    @Override
    public Employee findEmployeeByFingerprint(String fingerPrint) throws BackendException {
        return employeeRepo.findEmployeeByFingerprint(fingerPrint)
                            .orElseThrow(() -> new BusinessException("Employee doesn't exist"));
    }

    @Override
    public void updatePresenceCheckStatus(Integer employeeId, PresenceCheck checkStatus) throws BackendException {
        employeeRepo.updatePresenceCheckStatus(employeeId,checkStatus);
    }
}
