package com.stratio.challenge.entity;

import org.hibernate.annotations.Fetch;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * Entity class which represent access activity by
 * employee, department and access device
 *
 * @author charles
 * @version 1.0
 */
@Entity
@Table(name = "ACCESS_HISTORY")
public class AccessHistory implements Serializable {

    private static final long serialVersionUID = -1979863509240304792L;

    // =====================================================================
    // ATTRIBUTES
    // =====================================================================

    @EmbeddedId
    private AccessHistoryId accessHistoryId;

    /**
     * <b>IN</b>, when the employee enter in the department or
     * <b>OUT</b>, when the employee left the department
     */
    @Column(name = "CHECK_ACCESS", nullable = false)
    private String checkAccess;

    /**
     * Store <b>1</b> when the employee enter in the department or
     * <b>-1</b>, when the employee enter in the department
     */
    @Column(name = "FLAG_CHECK", nullable = false)
    private Integer flagCheck;

    /**
     * Record dateAccess of record when the employee entered or left the department
     */
    @Column(name = "DATE_ACCESS")
    private Date dateAccess;


    // =====================================================================
    // ACCESS METHODS
    // =====================================================================

    public AccessHistoryId getAccessHistoryId() {
        return accessHistoryId;
    }

    public void setAccessHistoryId(AccessHistoryId accessHistoryId) {
        this.accessHistoryId = accessHistoryId;
    }

    public Date getDateAccess() {
        return dateAccess;
    }

    public void setDateAccess(Date dateAccess) {
        this.dateAccess = dateAccess;
    }

    public String getCheckAccess() {
        return checkAccess;
    }

    public void setCheckAccess(String checkAccess) {
        this.checkAccess = checkAccess;
    }

    public Integer getFlagCheck() {
        return flagCheck;
    }

    public void setFlagCheck(Integer flagCheck) {
        this.flagCheck = flagCheck;
    }

}
