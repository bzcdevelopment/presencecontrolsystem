package com.stratio.challenge.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Entity class which represent the department's schedule
 *
 * @author charles
 * @version 1.0
 */
@Entity
@Table(name = "DEPARTMENT_SCHEDULE")
public class DepartmentSchedule implements Serializable {

    private static final long serialVersionUID = -3354210088516312472L;

    // =====================================================================
    // ATTRIBUTES
    // =====================================================================

    @EmbeddedId
    private DepartmentScheduleId departmentScheduleId;

    // =====================================================================
    // ACCESS METHODS
    // =====================================================================

    public DepartmentScheduleId getDepartmentScheduleId() {
        return departmentScheduleId;
    }

    public void setDepartmentScheduleId(DepartmentScheduleId departmentScheduleId) {
        this.departmentScheduleId = departmentScheduleId;
    }

    public Integer getDepartmentId(){
        return departmentScheduleId.getDepartmentId();
    }

    // =====================================================================
    // INNER CLASSES
    // =====================================================================




}
