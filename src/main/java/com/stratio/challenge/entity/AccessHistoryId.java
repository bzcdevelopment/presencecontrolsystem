package com.stratio.challenge.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AccessHistoryId implements Serializable {

    private static final long serialVersionUID = 5547164921387811936L;

    // =====================================================================
    // ATTRIBUTES
    // =====================================================================

    /**
     * Employee's identifier
     */
    @Column(name = Employee.ID_DBNAME, nullable = false)
    private Integer employeeId;

    /**
     * Department's identifier
     */
    @Column(name = Department.ID_DBNAME, nullable = false)
    private Integer departmentId;

    /**
     * Access device's identifier
     */
    @Column(name = AccessDevice.ID_DBNAME, nullable = false)
    private Integer accessDeviceId;

    // =====================================================================
    // CONSTRUCTORS
    // =====================================================================

    public AccessHistoryId() {}

    public AccessHistoryId(Integer employeeId, Integer departmentId, Integer accessDeviceId) {
        this();
        this.employeeId = employeeId;
        this.departmentId = departmentId;
        this.accessDeviceId = accessDeviceId;
    }

    // =====================================================================
    // ACCESS METHODS
    // =====================================================================


    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getAccessDeviceId() {
        return accessDeviceId;
    }

    public void setAccessDeviceId(Integer accessDeviceId) {
        this.accessDeviceId = accessDeviceId;
    }

    // =====================================================================
    // INNER IMPLEMENTATIONS
    // =====================================================================


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccessHistoryId that = (AccessHistoryId) o;
        return Objects.equals(getEmployeeId(), that.getEmployeeId()) &&
                Objects.equals(getDepartmentId(), that.getDepartmentId()) &&
                Objects.equals(getAccessDeviceId(), that.getAccessDeviceId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmployeeId(), getDepartmentId(), getAccessDeviceId());
    }
}