package com.stratio.challenge.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DepartmentScheduleId implements Serializable {

    private static final long serialVersionUID = -4502678175399221039L;

    // =====================================================================
    // ATTRIBUTES
    // =====================================================================

    /**
     * Department's identifier
     */
    private Integer departmentId;

    /**
     * Workdays's identifier
     */
    private Integer workdayId;

    // =====================================================================
    // CONSTRUCTORS
    // =====================================================================

    public DepartmentScheduleId() {}

    public DepartmentScheduleId(Integer departmentId, Integer workdayId) {
        this();
        this.departmentId = departmentId;
        this.workdayId = workdayId;
    }

    // =====================================================================
    // ACCESS METHODS
    // =====================================================================

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getWorkdayId() {
        return workdayId;
    }

    public void setWorkdayId(Integer workdayId) {
        this.workdayId = workdayId;
    }

    // =====================================================================
    // INNER IMPLEMENTATIONS
    // =====================================================================

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartmentScheduleId that = (DepartmentScheduleId) o;
        return Objects.equals(getDepartmentId(), that.getDepartmentId()) &&
                Objects.equals(getWorkdayId(), that.getWorkdayId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentId(), getWorkdayId());
    }
}
