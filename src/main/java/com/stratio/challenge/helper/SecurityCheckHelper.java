package com.stratio.challenge.helper;

import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.exception.ValidationException;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.function.Function;


@Component
public class SecurityCheckHelper {


    /**
     * @param isEnable
     * @throws ValidationException
     */
    public void isEmployeeEnabled(Boolean isEnable) throws ValidationException{
        if(!isEnable) {
            throw new ValidationException("Employee disabled");
        }
    }

    /**
     * @param check
     * @throws ValidationException
     */
    public void workdayTimeWindowValidator(PresenceCheck check, Calendar currentDate, Calendar checkInDate) throws ValidationException{

        boolean isValidTimeWindows;

        Function<String, ValidationException> getValidationExFn = (m) -> new ValidationException("Failed security check");

        try {
            switch (check) {
                case IN:
                    isValidTimeWindows = currentDate.after(checkInDate);
                    break;
                case OUT:
                    isValidTimeWindows = true;
                    break;
                default:
                    throw getValidationExFn.apply("Failed security check");
            }
        }catch(NullPointerException e){
            throw getValidationExFn.apply(e.getMessage());
        }

        if(!isValidTimeWindows){
            throw new ValidationException("The access to the apartment is from "+checkInDate.getTime());
        }
    }


    /**
     * @param currentCheckStatus
     * @param check
     * @throws ValidationException
     */
    public void employeePresenceCheckStatusValidator(PresenceCheck currentCheckStatus, PresenceCheck check) throws ValidationException{
        if(currentCheckStatus.equals(check)){
            throw new ValidationException("Invalid employee's presence status");
        }
    }
}
