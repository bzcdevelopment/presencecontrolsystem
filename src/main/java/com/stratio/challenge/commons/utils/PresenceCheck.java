package com.stratio.challenge.commons.utils;

public enum PresenceCheck {

    IN("IN",1),
    OUT("OUT",-1);

    private String label;
    private Integer flag;

    PresenceCheck(String label, Integer flag) {
        this.label = label;
        this.flag = flag;
    }

    public String getLabel(){
        return this.label;
    }

    public Integer getFlag(){
        return this.flag;
    }
}
