package com.stratio.challenge.commons.utils;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateUtils {

    // =====================================================================
    // ATTRIBUTES
    // =====================================================================

    public static final String yyyyMMdd_HHmmss = "yyyy-MM-dd HH:mm:ss";

    private static DateUtils INSTANCE;

    private static Object mutable = new Object();

    // =====================================================================
    // CONSTRUCTORS
    // =====================================================================

    private DateUtils() {}

    public static DateUtils getInstance(){
        if(INSTANCE == null){
            synchronized (mutable){
                INSTANCE = new DateUtils();
            }
        }
        return INSTANCE;
    }



    // =====================================================================
    // INNER IMPLEMENTATIONS
    // =====================================================================

    /**
     * @return
     */
    public final Calendar getCalendar(){
        return Calendar.getInstance();
    }

    /**
     * @return
     */
    public final Calendar.Builder getCalendarBuilder(){
        return new Calendar.Builder();
    }

    /**
     * @param time
     * @return
     */
    public Calendar getDateWithThisTime(final Time time){
        final Calendar date = getCalendar();
        return getCalendarBuilder().setDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE))
                                    .setFields(Calendar.HOUR, time.getHours())
                                    .setFields(Calendar.MINUTE, time.getMinutes())
                                    .setFields(Calendar.SECOND, time.getSeconds())
                                    .build();
    }

    /**
     * @param date
     * @param format
     * @return
     * @throws ParseException
     */
    public final Calendar getDateFromString(final String date, final String format) throws ParseException {
        return getCalendarBuilder().setInstant(new SimpleDateFormat(format, Locale.getDefault()).parse(date)).build();
    }
}
