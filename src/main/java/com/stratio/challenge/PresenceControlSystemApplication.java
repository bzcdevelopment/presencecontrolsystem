package com.stratio.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PresenceControlSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(PresenceControlSystemApplication.class, args);
    }

}
