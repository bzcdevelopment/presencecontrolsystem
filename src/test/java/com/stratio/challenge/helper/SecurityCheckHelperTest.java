package com.stratio.challenge.helper;


import com.stratio.challenge.commons.utils.DateUtils;
import com.stratio.challenge.commons.utils.PresenceCheck;
import com.stratio.challenge.exception.ValidationException;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class SecurityCheckHelperTest {

    // =====================================================================
    // ATTRIBUTES
    // =====================================================================

    @Autowired
    private SecurityCheckHelper SecurityCheckHelper;

    // =====================================================================
    // UNIT TEST
    // =====================================================================

    @Test
    public void isEmployeeEnabledIsOk() throws ValidationException {
        SecurityCheckHelper.isEmployeeEnabled(true);
        Assert.assertTrue(true);
    }


    @Test(expected = ValidationException.class)
    public void isEmployeeEnabledFailThrowingValidationException() throws ValidationException {
        SecurityCheckHelper.isEmployeeEnabled(false);
    }

    @Test(expected = ValidationException.class)
    public void workdayTimeWindowValidatorThrowingValidationExceptionWhenCheckArgIsNull() throws ValidationException {
        try{
        SecurityCheckHelper.workdayTimeWindowValidator( null,
                                                        DateUtils.getInstance().getCalendar(),
                                                        DateUtils.getInstance().getDateFromString("2019-07-21 9:01:00", DateUtils.yyyyMMdd_HHmmss));
        }catch (ParseException e){
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void workdayTimeWindowValidatorValidCheckInTimeWindow() throws ValidationException {

        try {
            SecurityCheckHelper.workdayTimeWindowValidator( PresenceCheck.IN,
                                                            DateUtils.getInstance().getDateFromString("2019-07-21 9:01:00", DateUtils.yyyyMMdd_HHmmss),
                                                            DateUtils.getInstance().getDateFromString("2019-07-21 9:00:00", DateUtils.yyyyMMdd_HHmmss));

        }catch (ParseException e){
            Assert.fail(e.getMessage());
        }
    }


    // =====================================================================
    // INNER CLASSES
    // =====================================================================
    @Configuration
    static class TestConfigurationContext{

        @Bean
        public SecurityCheckHelper securityCheckHelper(){
            return new SecurityCheckHelper();
        }

    }

}








