# PresenceControlSystem

Design and implement the backend for a presence control system for the employees of a
company

Endpoint definidos para marcar las entradas y salidas al departamento

Entradas:

  Registra el acceso de un empleado a un departamento 

curl -X POST \
  http://localhost:8080/access/in \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Postman-Token: 7036b043-bcc6-42c4-8bd1-e4ba97534eb9' \
  -H 'cache-control: no-cache' \
  -d 'fingerprint=FP1&accessDeviceId=1&undefined='
  
Salidas:

  Registra la salida de un empleado a un departamento 

curl -X POST \
  http://localhost:8080/access/out \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Postman-Token: 328677a8-6e77-4c91-bbe2-6630d44a8782' \
  -H 'cache-control: no-cache' \
  -d 'fingerprint=FP1&accessDeviceId=2&undefined='
